package de.grundid.NFCPay;

import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;
import android.util.Log;

/**
 * Klasse die den Host APDU Service von Android benutzt um APDU Commands zu verarbeiten.
 */
public class MyHostApduService extends HostApduService {

    /**
     * Wird aufgerufen wenn CommandApdu eines anderen Gerätes empfangen wird.
     * Verarbeitet commandApdu
     *
     * @param apdu   empfangener APDU Befehl
     * @param extras unbenutzt, noetig wegen HostApduService
     * @return Nachricht an den PC / das Gegengerät
     */
    @Override
    public byte[] processCommandApdu(byte[] apdu, Bundle extras) {

        /*
         * Kassenobjekt zur Transaktionssteuerung
         */
        Kasse kasse = Kasse.getInstance();

        /*
         * Wenn empfangener Befehl mit 0x00 0xa4 anfaengt
         * d.h. ein Interindusty Select Befehl ist
         * sende Grussnachricht
         */
        if (apdu.length >= 2 && apdu[0] == (byte) 0 && apdu[1] == (byte) 0xa4) {
            Log.i("HCEDEMO", "Application selected");
            return "Hello".getBytes();
        }
        /*
         * Sonst Standardnachricht
         */
        else {
            Log.i("HCEDEMO", "Received: " + new String(apdu));
            return (kasse.bezahlvorgang(new String(apdu))).getBytes();
            /*
             * Nach letzter Nachricht
             */
        }
    }

    /*
     *  Muss wegen HostApduService implementiert werden.
     *  Hat aber keine Funktion ausser Log.
     *  Laut Android Developers:
     *  This method will be called in two possible scenarios:
     *       The NFC link has been deactivated or lost
     *       A different AID has been selected and was resolved to a different service component
     */
    @Override
    public void onDeactivated(int reason) {
        Log.i("HCEDEMO", "Deactivated: " + reason);
    }
}