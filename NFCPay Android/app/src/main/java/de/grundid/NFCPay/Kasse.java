package de.grundid.NFCPay;

import android.widget.TextView;

/**
 * Klasse zur Bereitstellung der Kassenlogik und Transaktionssteuerung.
 */
public class Kasse {

    /*
     * Klassenobjekt als Singleton
     */
    private static Kasse instance;
    /*
     * Daten der letzten Transaktion.
     * Erste Feld hat ID, zweite Wert
     * Ggf auch wie guthaben speichern
     */
    String[] letzteTransaktion;
    /*
     * Aktuelle Transaktionsdaten
     */
    String[] aktuelleTransaktion;
    /*
     * MainAktivity der APP, workaround fuer Guthabenaktualisierung
     */
    private MainActivity mainActivity;
    /*
     * Geldbetrag im Geldbeutel
     */
    private double guthaben;

    /**
     * Konstruktor der Kasse
     */
    private Kasse() {
        /*
         * Startguthaben, sollte durch Laden eines existierenden Betrags ersetzt werden.
         */
        letzteTransaktion = new String[2];
    }

    /**
     * Getter Fuer Singleton der Kasse.
     *
     * @return Instanz der Kasse
     */
    public static Kasse getInstance() {
        if (Kasse.instance == null) {
            Kasse.instance = new Kasse();
        }
        return Kasse.instance;
    }

    /**
     * Methode um der Kasse die MainActivity zu geben. Workaround (siehe oben)
     */
    public void setMainActivity(MainActivity activity) {
        mainActivity = activity;
    }

    /**
     * Mehtode zum Aufladen des Guthabens im Geldbeutel.
     */
    public void guthabenAufladen(double menge) {
        guthaben += menge;
    }

    /**
     * Methode zum lesen des Guthabens;
     */
    public double getGuthaben() {
        return guthaben;
    }

    /**
     * Methode zum setzen des Guthabens auf deinen bestimmten Wert.
     */
    public void setGuthaben(double wert) {
        guthaben = wert;
    }

    /**
     * Methode zum Iterieren durch die Schritte der Transaktion.
     *
     * @param apdu Per NFC erhaltene APDU
     * @return Nachricht die an das Gegengeraet versendet wird.
     */
    public String bezahlvorgang(String apdu) {
        String nachricht = "ERROR";
        double wert;

        /* Regex kommt Parsing Fehlern zuvor.
         *
         * Regex prueft auf:
         * String beginnt mit ID
         * gefolgt von bel. vielen Zahlen,
         * gefolgt von Whitespace,
         * gefolgt von bel. vielen Ziffern,
         *  ggf gefolgt von Punkt
         *      gefolgt von 0 bis 2 Ziffern
         *
         * Sendet dann den Selben String als bestaetigung zurueck.
         *
         * String besteht aus ID und Betrag
         * ID besteht aus "ID"+Ziffern
         */
        if (apdu.matches("^ID\\d+\\s\\d+(\\.\\d{0,2})?$")) {
            aktuelleTransaktion = apdu.split("\\s");
            if (aktuelleTransaktion[1].equals(letzteTransaktion[1])) {
                nachricht = "Schon bezahlt!";
            }
            else {
                nachricht = apdu;
            }
        }
        /*
         * Erhalt der Bestaetigung wird bestaetigt, hier sollte dann Geld abgebucht werden
         */
        else if (apdu.equals("OK")) {
            wert = Double.parseDouble(aktuelleTransaktion[1]);
            if (wert > guthaben) {
                nachricht = "Nicht genug Guthaben";
            }
            else {
                guthaben -= wert;
                /*
                 * Vorsicht! Bei echten Geldwerten niemals runden und niemals double verwenden!
                 * Zur der Einfachheit halber aber so gemacht!
                 */
                guthaben = Math.round(guthaben * 100) / 100.0;
                letzteTransaktion = aktuelleTransaktion;
                nachricht = "OK";
            }
        }
        else {
            nachricht = "END";
        }

        TextView guthabenAnzeige = (TextView) mainActivity.findViewById(R.id.textView);
        guthabenAnzeige.setText(Double.toString(this.getGuthaben()));

        return nachricht;
    }

}
