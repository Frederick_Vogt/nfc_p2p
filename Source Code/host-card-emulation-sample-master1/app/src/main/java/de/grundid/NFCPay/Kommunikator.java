package de.grundid.NFCPay;

import android.nfc.tech.IsoDep;

import java.io.IOException;

/**
 * Klasse fuer die Kommunikation ueber NFC.
 */
public class Kommunikator implements Runnable {


    /*
     *  Zu sendende Nachricht als APDU.
	 *  Aufgebaut nach ISO 7816-4 8.1.1
	 *  0x00 = Befehlsklasse (0x00 = interindustry command set)
	 *  0xA4 = Kommando (0xA4 = SELECT command)
	 *  0x04 = 1. Parameter fuer Kommando (0x04 = select by file ID)
	 *  0x00 = 2. Parameter fuer Kommando (0x00 = ??)
	 *
	 *  vgl.: http://de.wikipedia.org/wiki/Application_Protocol_Data_Unit
	 */
    private static byte[] APDU_COMMAND = {0x00, (byte) 0xA4, 0x04, 0x00};
    // APDU-ID der Android App mit der Kommuniziert werden soll.
    // Wird in der apduservice.xml der App festgelegt.
    // <aid-filter android:name="F0010203040506" />
    private static byte[] AID = {(byte) 0xF0, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06};
    private IsoDep isoDep;
    private OnMessageReceived onMessageReceived;

    /**
     * Konstruktor des NFC Kommunikators.
     * Besitzt auch Sucher-Funktionalitaet fuer die Uebertragung (vgl. Code fuer Desktop)
     * da Smartphone nie aktiv Verbindet und daher nicht nach Verbindungen suchen muss.
     *
     * @param isoDep            Adapter
     * @param onMessageReceived Signal wenn Nachricht erhalten wird
     */
    public Kommunikator(IsoDep isoDep, OnMessageReceived onMessageReceived) {
        this.isoDep = isoDep;
        this.onMessageReceived = onMessageReceived;
    }

    /**
     * Methode zum Erstellen des zu sendenden select-Befehls.
     * Besteht aus APDU_COMMAND und AID und koennte auch fertig
     * per Hand geschrieben werden, Methode vereinfacht aber das Anpassen
     * bei Aenderungen der AID oder des APDU_COMMAND.
     *
     * @param aid Android APDU-ID
     * @return Fertiger select-Befehl
     */
    private byte[] createSelect(byte[] aid) {
		/*
		 *  Befehl besteht aus 6 Command Bytes (APDU_COMMAND) und der AID (variable Laenge je nach AID)
		 */
        byte[] befehl = new byte[6 + aid.length];

        // Kopieren des APDU_COMMANDs
        System.arraycopy(APDU_COMMAND, 0, befehl, 0, APDU_COMMAND.length);
        befehl[4] = (byte) aid.length;

        // Kopieren der AID
        System.arraycopy(aid, 0, befehl, 5, aid.length);
        befehl[befehl.length - 1] = 0;
        return befehl;
    }

    /**
     * Methode zum Aufbau der P2P Kommunikation als Initiator.
     * Sollte erstmal nicht benutzt werden da PC immer initiiert.
     */
    @Override
    public void run() {
        int nachrichtenzaehler = 0;
        /*
         * Versuche zu verbinden...
         */
        try {
            isoDep.connect();

            /*
             * erstelle Select Befehl
             */
            byte[] response = isoDep.transceive(createSelect(AID));

            /*
             * Waehrend Verbindung besteht...
             */
            while (isoDep.isConnected() && !Thread.interrupted()) {
                /*
                 * Srstelle Nachricht
                 */
                String message = "Message from IsoDep " + nachrichtenzaehler++;
                /*
                 * Sende Nachricht und empfange Antwort (?)
                 */
                response = isoDep.transceive(message.getBytes());
                /*
                 * Sende Antwort wenn Nachricht erhalten wurde (?)
                 */
                onMessageReceived.onMessage(response);
            }
            isoDep.close();
        } catch (IOException e) {
            /*
             * Fehlerbehandlung
             */
            onMessageReceived.onError(e);
        }
    }

    public interface OnMessageReceived {

        void onMessage(byte[] message);

        void onError(Exception exception);
    }
}
