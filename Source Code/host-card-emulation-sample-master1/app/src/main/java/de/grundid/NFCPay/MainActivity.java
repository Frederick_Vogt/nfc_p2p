package de.grundid.NFCPay;

import android.app.Activity;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.ReaderCallback;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import de.grundid.NFCPay.Kommunikator.OnMessageReceived;

/**
 * Android Main Activity Klasse, entspricht etwa der "Main" in normalem Java.
 * Beschreibt die Behandlung verschiedener Systemsignale wie OnCreate fuer die erstmalige
 * Programmausfuehrung und OnPause fuer die Pausierung der App.
 */
public class MainActivity extends Activity implements OnMessageReceived, ReaderCallback {

    /*
     * NFC Adapter
     */
    private NfcAdapter nfcAdapter;

    /**
     * Methode die beim Starten der App/Activity aufgerufen wird und ihr Verhalten regelt.
     *
     * @param savedInstanceState Moeglicherweise gespeicherte vorherige Ausfuehrung der App
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
         * Laden des Guthabens aus vorherigen Appausfuehrungen.
         */
        SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
        // 0 ist default bei getLong wenn "guthaben" als key nicht in den Preferences ist
        double guthaben = Double.longBitsToDouble(sharedPref.getLong("guthaben", 0));

        Kasse kasse = Kasse.getInstance();
        /*
         * Guthaben der Kasse auf "alten" Wert setzen
         */
        kasse.setGuthaben(guthaben);
        /*
         * Main Activity an die Kasse uebergeben.
         * Workaround damit Guthaben in der GUI ausserhalb der MainActivity geaendert werden kann.
         */
        kasse.setMainActivity(this);

        /*
         * Hauptfenster
         */
        setContentView(R.layout.activity_main);
        /*
         * Hauptfenster in den TextView Modus setzen.
         */
        TextView guthabenAnzeige = (TextView) findViewById(R.id.textView);
        /*
         * Guthaben anzeigen
         */
        guthabenAnzeige.setText(Double.toString(kasse.getGuthaben()));

        /*
         * Adapter verwenden.
         */
        nfcAdapter = NfcAdapter.getDefaultAdapter(this); // Default Adapter verwenden
    }

    /**
     * Methode die das aktuelle Guthaben in den Shared Preferences speichert.
     */
    public void guthabenSpeichern() {
        /*
         * Shared Preferences und einen Editor fuer diese erstellen
         */
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        Kasse kasse = Kasse.getInstance();
        /*
         * Guthaben mit dem Key "guthaben" in den Shared Preferences speichern.
         */
        editor.putLong("guthaben", Double.doubleToLongBits(kasse.getGuthaben()));
        /*
         * Aenderungen an den Shared Preferences ausfuehren.
         */
        editor.apply();
    }

    /**
     * Methode, die beim Stoppen der App aufgerufen wird.
     */
    @Override
    public void onStop() {
        /*
         * Speichern des vorhandenen Guthabens.
         */
        guthabenSpeichern();

        super.onStop();
    }

    /**
     * Methode, die beim Zerstoeren der App aufgerufen wird.
     */
    @Override
    public void onDestroy() {
        /*
         * Speichern des vorhandenen Guthabens.
         */
        guthabenSpeichern();

        super.onDestroy();
    }

    /**
     * Methode die das Verhalten beim Wiederaufnehmen/Refokussieren der App regelt.
     */
    @Override
    public void onResume() {
        super.onResume();
        /*
         * ReaderMode deaktivieren damit Daten empfangen werden koennen.
         * Sonst kann App nur im Hintergrund / Unfokussiert empfangen.
         */
        nfcAdapter.disableReaderMode(this);
    }

    /**
     * Methode die das Verhalten der App beim Pausieren/Defokussieren regelt.
     * Könnte hier das Problem mit "App darf nicht fokussiert sein" liegen?
     */
    @Override
    public void onPause() {
        super.onPause();
        /*
         * Speichern des vorhandenen Guthabens.
         */
        guthabenSpeichern();

        /*
         * NFC reader Modus beenden.
         */
        nfcAdapter.disableReaderMode(this);
    }

    /**
     * Methode zum Verhalten bei Entdecken eines NFC Tags.
     *
     * @param tag Tag in der Naehe? Erforderlich, hat interne Funktion?
     */
    @Override
    public void onTagDiscovered(Tag tag) {
        /*
         * ISO DEP regelt Kommunikation nach ISO 14443-4
         */
        IsoDep isoDep = IsoDep.get(tag);

        /*
         * kommunikator Sendet und erhaelt Daten
         * this bezieht sich hier auf die erhaltene Message, deswegen muss auch onMessageReceived
         * implementiert werden.
         */
        Kommunikator kommunikator = new Kommunikator(isoDep, this);

        /*
         * Neuen Thread fuer die NFC IO
         */
        Thread thread = new Thread(kommunikator);
        thread.start();
    }

    /**
     * Methode die beim Button druecken aufgerufen wird und das Guthaben um 10 Euro erhoeht.
     * Ist auch eine Signalaufgerufene Methode, treffender Name waer auch
     * onButtonGuthabenErhoehtPressed
     * um konsistent zu bleiben. Allerdings ist der Name sehr sperrig.
     *
     * @param view Erforderlich fuer Button Signale
     */
    public void erhoeheGuthaben(View view) {
        /*
         * Guthaben aufladen.
         */
        Kasse kasse = Kasse.getInstance();
        kasse.guthabenAufladen(10.0);
        /*
         * Guthaben auf der GUI aktualisieren.
         */
        TextView guthabenAnzeige = (TextView) findViewById(R.id.textView);
        guthabenAnzeige.setText(Double.toString(kasse.getGuthaben()));
    }

    /**
     * Wenn Message von Initiator, hier PC, erhalten wird wird diese Methode ausgefuehrt.
     * Hat keine Funktion aber wird verlangt.
     */
    @Override
    public void onMessage(final byte[] message) {
    }

    /**
     * Methode die das Verhalten im Fehlerfall bestimmt.
     * Hat keine Funktion aber wird verlangt.
     *
     * @param exception geworfene Exception
     */
    @Override
    public void onError(Exception exception) {
    }
}
