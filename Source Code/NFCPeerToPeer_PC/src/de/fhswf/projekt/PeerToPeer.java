/**
 * 
 */
package de.fhswf.projekt;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.smartcardio.CardTerminal;

import org.nfctools.examples.TerminalUtils;

/**
 * Main Klasse der NFC P2P Kommunikation.
 */
public class PeerToPeer {

	/**
	 * Methode zum Aufbau der P2P Kommunikation.
	 */
	// Da Methoden der deprecated smartcardio genutzt werden muessen/sollten
	// Warnungen suppressed werden.
	@SuppressWarnings("deprecation")
	public void run() {
		/*
		 * Error und Transaktionslog anlegen bzw SessionStart eintragen
		 */
		try {
			String timestamp = "SessionStart " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
					.format(new Date());

			/*
			 * Zweiter parameter von FileWriter (true) haengt Text von println
			 * an existierende Datei an.
			 */
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter("transaktionen.txt", true)));
			out.println(timestamp);
			out.close();
			/*
			 * Zweiter parameter von FileWriter (true) haengt Text von println
			 * an existierende Datei an.
			 */
			out = new PrintWriter(new BufferedWriter(new FileWriter("log.txt",
					true)));
			out.println(timestamp);
			out.close();
		} catch (IOException exception) {
			exception.printStackTrace();
		}

		// Beschaffen der vorhandenen Smartcard/NFC Terminals. Im Regelfall nur
		// eins.
		CardTerminal cardTerminal = TerminalUtils.getAvailableTerminal()
				.getCardTerminal();

		/*
		 * Anlegen eines neuen p2pSuchers fuer den P2P Betrieb. Der Sucher
		 * versucht so lange aktiv P2P Verbindungen aufzubauen, bis ein
		 * NFCGeraet mit P2P Faehigkeit gefunden wird, dann wird die Verbindung
		 * aufgebaut.
		 */
		P2PSucher sucher = new P2PSucher(cardTerminal);

		// Starten des Suchers
		sucher.run();
	}

	/**
	 * Main Methode zum Aufbau der P2P Verbindung. Ruft run() auf.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new PeerToPeer().run();
	}

}
