package de.fhswf.projekt;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;

import org.nfctools.NfcException;
import org.nfctools.api.ApduTag;
import org.nfctools.api.Tag;
import org.nfctools.api.TagType;
import org.nfctools.scio.Command;
import org.nfctools.scio.Response;
import org.nfctools.spi.acs.Acs;

/**
 * Klasse fuer "Simulation" eines P2P faehiges NFC Tag.
 */
public class P2PTag extends Tag implements ApduTag {

	private Card card;

	/**
	 * Konstruktor des P2P NFC tags.
	 * @param tagType Art des NFC Tags
	 * @param generalBytes Wird vom Superkonstruktor Tag verlangt, scheint aber keine Funktion zu haben
	 * @param card NFC Karten Objekt
	 */
	public P2PTag(TagType tagType, byte[] generalBytes, Card card) {
		// Superkonstruktoraufruf des Tag
		super(tagType, generalBytes);
		this.card = card;
	}

	/**
	 * Methde zum Senden von APDU Befehlen und Empfangen der Antworten.
	 * @param command Befehl, der zur Karte gesendet wird
	 * @return Antwort der Karte auf den Befehl
	 */
	public Response transmit(Command command) {
		try {
			// Neuen APDU Befehl erstellen
			CommandAPDU commandAPDU = new CommandAPDU(0xff, 0, 0, 0, command.getData(), command.getOffset(), command.getLength());
			
			/*
			 * Befehl an Karte senden; Hier liegt wohl auch eines der Probleme warum der ACS1252 nicht unterstuetzt wurde:
			 * Es gibt kein passendes Escape Command!
			 */
			byte[] transmitControlResponse = card.transmitControlCommand(Acs.IOCTL_SMARTCARD_ACR122_ESCAPE_COMMAND,
					commandAPDU.getBytes());
			
			// Antwort auf Befehl erstellen
			ResponseAPDU responseAPDU = new ResponseAPDU(transmitControlResponse);
			
			// Antwort auf Befehl returnen
			return new Response(responseAPDU.getSW1(), responseAPDU.getSW2(), responseAPDU.getData());
		}
		catch (CardException exception) {
			/*
			 * Fehlerbehandlung
			 */
			throw new NfcException(exception);
		}
	}
}
