package de.fhswf.projekt;

import java.util.Random;

/**
 * Klasse zur Bereitstellung der Kassenlogik und Transaktionssteuerung.
 */
public class Kasse {

	/*
	 * Klassenobjekt als Singleton
	 */
	private static Kasse instance;

	/*
	 * ID fuer den Bezahlvorgang
	 */
	private String bezahlID = "";

	/*
	 * Betrag fuer die aktuelle Treansaktion
	 */
	private String betrag = "";
	
	/*
	 * letzte erhaltene Nachricht
	 */
	private String letzteNachricht = "";

	/**
	 * Konstruktor der Kasse.
	 * Private, da Singleton.
	 */
	private Kasse() {
	}

	/**
	 * Getter Fuer Singleton der Kasse.
	 * 
	 * @return Instanz der Kasse
	 */
	public static Kasse getInstance() {
		if (Kasse.instance == null) {
			Kasse.instance = new Kasse();
		}
		return Kasse.instance;
	}
	
	/**
	 * Methode die die letzte erhaltene Nachricht (apdu) wiedergibt.
	 * Kasse bietet sich daf�r an, da sie ein Singleton ist.
	 * @return letzte erhaltene Nachricht
	 */
	public String getLetzteNachricht () {
		return letzteNachricht;
	}

	/**
	 * Methode zum Setzen des zu bezahlenden Betrags.
	 * 
	 * @param betr 	Betrag, der bezahlt werden muss.
	 * 				Ungueltige Eingaben werden auf Smartphoneseite verworfen.
	 * @return Bezahl-ID des Vorgangs
	 */
	public String setBetrag(String betr) {
		betrag = betr.replace(",", ".");
		/*
		 * Neue Transaktions-ID erstellen. 
		 * nextInt(Integer.MAX_VALUE) sorgt dafuer, dass nur positive Werte entstehen.
		 */
		bezahlID = "ID" + Integer.toString(new Random().nextInt(Integer.MAX_VALUE));
		return bezahlID;
	}

	/**
	 * Methode zum Behandeln der Transaktion.
	 * Wertet erhaltene Nachrichten aus und sendet entsprechende Antworten.
	 * 
	 * @param apdu Nachricht die erhalten wird.
	 * @return Nachricht die an das Gegengeraet versendet wird.
	 */
	public String bezahlvorgang(String apdu) {
		String nachricht = "ERROR";
		
		letzteNachricht = apdu;

		/*
		 * Bei erster Uebermittlung (keine response vorhanden) sende bezahlID und betrag
		 */
		if (apdu.equals("")) {
			nachricht = bezahlID + " " + betrag;
		}
		/*
		 * Wenn gesendete Daten korrekt zurueck kommen sende "Bezahlvorgang abgeschlossen", d.h. OK
		 */
		else if (apdu.equals(bezahlID + " " + betrag))
		{
			nachricht = "OK";
			// Transaktion speichern.
			Buchfuehrung.transaktionSpeichern(apdu);
			System.out.println("	Transaktion erfolgreich!");
		}
		/*
		 * Transaktionsende
		 */
		else
		{
			nachricht = "END";
		}

		return nachricht;
	}

}
