package de.fhswf.projekt;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.nfctools.io.ByteArrayReader;
import org.nfctools.io.ByteArrayWriter;
import org.nfctools.spi.tama.AbstractTamaCommunicator;
import org.nfctools.spi.tama.request.DataExchangeReq;
import org.nfctools.spi.tama.request.InListPassiveTargetReq;
import org.nfctools.spi.tama.response.DataExchangeResp;
import org.nfctools.spi.tama.response.InListPassiveTargetResp;

/**
 * Klasse fuer die Kommunikation ueber NFC.
 */
public class Kommunikator extends AbstractTamaCommunicator {

	/*
	 * Zu sendende Nachricht als APDU. Aufgebaut nach ISO 7816-4 8.1.1 0x00 =
	 * Befehlsklasse (0x00 = interindustry command set) 0xA4 = Kommando (0xA4 =
	 * SELECT command) 0x04 = 1. Parameter fuer Kommando (0x04 = select by file
	 * ID) 0x00 = 2. Parameter fuer Kommando (0x00 = ??)
	 * 
	 * vgl.: http://de.wikipedia.org/wiki/Application_Protocol_Data_Unit
	 */
	private static byte[] APDU_COMMAND = { 0x00, (byte) 0xA4, 0x04, 0x00 };

	// APDU-ID der Android App mit der Kommuniziert werden soll.
	// Wird in der apduservice.xml der App festgelegt.
	// <aid-filter android:name="F0010203040506" />
	private static byte[] AID = { (byte) 0xF0, 0x01, 0x02, 0x03, 0x04, 0x05,
			0x06 };

	/**
	 * Konstruktor des NFC Kommunikators. Bei P2P sind reader und writer
	 * identisch!
	 * 
	 * @param reader
	 *            Reader fuer die Kommunikation.
	 * @param writer
	 *            Writer fuer die Kommunikation.
	 */
	public Kommunikator(ByteArrayReader reader, ByteArrayWriter writer) {
		// Superkonstruktoraufruf AbstractTamaCommunicator
		super(reader, writer);
	}

	/**
	 * Methode zum Erstellen des zu sendenden select-Befehls. Besteht aus
	 * APDU_COMMAND und AID und koennte auch fertig per Hand geschrieben werden,
	 * Methode vereinfacht aber das Anpassen bei Aenderungen der AID oder des
	 * APDU_COMMAND.
	 * 
	 * @param aid
	 *            Android APDU-ID
	 * @return Fertiger select-Befehl
	 */
	private byte[] erstelleSelectAPDU(byte[] aid) {
		/*
		 * Befehl besteht aus 6 Command Bytes (APDU_COMMAND) und der AID
		 * (variable Laenge je nach AID)
		 */
		byte[] befehl = new byte[6 + aid.length];

		// Kopieren des APDU_COMMANDs
		System.arraycopy(APDU_COMMAND, 0, befehl, 0, APDU_COMMAND.length);
		befehl[4] = (byte) aid.length;

		// Kopieren der AID
		System.arraycopy(aid, 0, befehl, 5, aid.length);
		befehl[befehl.length - 1] = 0;

		// Befehl enthaelt jetzt APDU und AID in korrekter Reihenfolge
		return befehl;
	}

	/**
	 * Verbindungsaufbau als Initiator fuer P2P Kommunikation.
	 * 
	 * @throws IOException
	 */
	public void verbindeAlsInitiator() throws IOException {
		while (true) {
			/*
			 * Erstelle Liste aller moeglichen Ziele in Reichweite (NFC
			 * Geraete/Tags)
			 */
			InListPassiveTargetResp inListPassiveTargetResp = sendMessage(new InListPassiveTargetReq(
					(byte) 1, (byte) 0, new byte[0]));
			/*
			 * Pruefe, ob die oben erstellte Liste groesser 1 ist, d.h. ein
			 * Geraet ist vorhanden.
			 */
			if (inListPassiveTargetResp.getNumberOfTargets() > 0) {
				/*
				 * Wenn ISO DEP I/O vom Geraet unterstuetzt wird...
				 */
				if (inListPassiveTargetResp.isIsoDepSupported()) {
					// APDU Befehl erstellen
					byte[] befehl = erstelleSelectAPDU(AID);

					// Befehl senden
					DataExchangeResp resp = sendMessage(new DataExchangeReq(
							inListPassiveTargetResp.getTargetId(), false,
							befehl, 0, befehl.length));

					// Daten erhalten
					String dataIn = new String(resp.getDataOut());

					/*
					 * Wenn erhaltene Daten mit "Hello" beginnen... Prueft
					 * korrekte uebertragung, kann bel. Code/Wort sein, solange
					 * es auf beiden Seiten identisch ist!
					 */

					if (dataIn.startsWith("Hello")) {
						// tausche Daten aus
						datenaustausch(inListPassiveTargetResp);
					}
				}
				/*
				 * ... sonst Fehler
				 */
				else {
					System.out.println("ISO DEP nicht unterstuetzt!");
				}
				break;
			} else {
				/*
				 * warten
				 */
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

	/**
	 * Methode zum Datenaustausch mit einem NFC Geraet ueber P2P per HCE.
	 * dataOut enthaelt die uebertragenen Nutzdaten. dataIn enthaelt die
	 * empfangenen Nutzdaten.
	 * 
	 * @param inListPassiveTargetResp
	 *            Geraet fuer den Datenaustausch
	 */
	/*
	 * Haette auch als
	 * 	private byte[] mit return resp.getDataOut()
	 * implementiert werden koennen
	 */
	private void datenaustausch(InListPassiveTargetResp inListPassiveTargetResp)
			throws IOException {
		DataExchangeResp resp;
		String dataIn = "";
		Kasse kasse = Kasse.getInstance();
		/*
		 * While Schleife pro zu sendender Nachricht 1 mal
		 */
		while (true) {
			/*
			 * Zu sendende Payload. Wird durch Kasse bestimmt.
			 */
			byte[] dataOut = (kasse.bezahlvorgang(dataIn)).getBytes();

			// Senden der Nachricht
			resp = sendMessage(new DataExchangeReq(
					inListPassiveTargetResp.getTargetId(), false, dataOut, 0,
					dataOut.length));

			// Erhaltene Daten
			dataIn = new String(resp.getDataOut());

			/*
			 * Logging der erhaltenen Daten.
			 * Doppelte Nachrichten werden gefiltert um viele aufeinanderfolgende "END"
			 * im Log zu vermeiden.
			 * 
			 * Zweiter parameter von FileWriter (true) haengt Text von println
			 * an existierende Datei an.
			 */
			if (!dataIn.equals(Kasse.getInstance().getLetzteNachricht())) {
				PrintWriter out = new PrintWriter(new BufferedWriter(
						new FileWriter("log.txt", true)));
				out.println(dataIn);
				out.close();
			}
		}
	}
}
