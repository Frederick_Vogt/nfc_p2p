package de.fhswf.projekt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;

import org.nfctools.api.TagType;
import org.nfctools.spi.acs.AbstractTerminalTagScanner;
import org.nfctools.spi.acs.ApduTagReaderWriter;

/**
 * Klasse die den P2P Sucher, d.h. aktiven Tag Scanner beschreibt.
 */
public class P2PSucher extends AbstractTerminalTagScanner {

	/**
	 * Konstruktor des P2PSuchers.
	 * 
	 * @param cardTerminal
	 *            per smartcardio gefundenes Terminal.
	 */
	protected P2PSucher(CardTerminal cardTerminal) {
		// Aufruf des Superconstruktors
		super(cardTerminal);
	}

	/**
	 * Methode, die alle gespeicherten Debugging Logs ausgibt.
	 * 
	 * @return String mit allen gespeicherten Transaktionen.
	 */
	public static String getLog() {
		String transaktionen = "EMPTY";
		try {
			transaktionen = new String(Files.readAllBytes(Paths.get("log.txt")));
		} catch (IOException exception) {
			exception.printStackTrace();
		}

		return transaktionen;
	}

	/**
	 * Auffordern des Nutzers zur Eingabe, fuehrt entweder zur Anzeige von
	 * Transaktionslog oder Betragseingabe.
	 * 
	 */
	private void eingabeaufforderung() {
		Kasse kasse = Kasse.getInstance();
		BufferedReader bufferedreader = new BufferedReader(
				new InputStreamReader(System.in));
		boolean transaktion = false;
		String eingabe = "";

		while (!transaktion) {
			/*
			 * Menu ausgeben
			 */
			System.out.println("\nWelche Aktion soll ausgefuehrt werden?");
			System.out.println("	(T)ransaktion durchfuerehn");
			System.out.println("	(L)og aller Transaktionen anzeigen");
			System.out
					.println("	(D)ebugging-Log der erhaltenen Nachrichten anzeigen");
			System.out.println("	(E)xit");
			System.out.println("Eingabe: ");

			try {
				eingabe = bufferedreader.readLine();
			} catch (IOException exception) {
				exception.printStackTrace();
			}

			/*
			 * Bei L fuer Transaktionslog
			 */
			if (eingabe.equals("L") || eingabe.equals("l")) {
				Buchfuehrung.printTransaktionen();
				/*
				 * Schleifenwiederholung per Eingabe
				 */
				System.out.println("	Weiter mit beliebiger Eingabe.");
				try {
					bufferedreader.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			/*
			 * Bei T fuer Transaktion
			 */
			else if (eingabe.equals("T") || eingabe.equals("t")) {
				System.out.println("Betrag: ");
				try {
					/*
					 * Betrag an Kasse uebergeben
					 */
					eingabe = bufferedreader.readLine();
					kasse.setBetrag(eingabe);
					System.out.println("Halten sie das Smartphone jetzt an den NFC Reader/Writer.");
				} catch (IOException e) {
					e.printStackTrace();
				}
				transaktion = true;
			}
			/*
			 * Bei D fuer Debugging Log
			 */
			else if (eingabe.equals("D") || eingabe.equals("d")) {
				System.out.println(getLog());
				/*
				 * Schleifenwiederholung per Eingabe
				 */
				System.out.println("	Weiter mit beliebiger Eingabe.");
				try {
					bufferedreader.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			/*
			 * Bei E fuer Exit
			 */
			else if (eingabe.equals("E") || eingabe.equals("e")) {
				System.exit(0);
			}
			/*
			 * Bei falscher Eingabe
			 */
			else {
				System.out.println("Ungueltige Eingabe!");
				/*
				 * Schleifenwiederholung per Eingabe
				 */
				System.out.println("	Weiter mit beliebiger Eingabe.");
				try {
					bufferedreader.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Methode zum Suchen und Aufbauen von P2P Verbindungen.
	 */
	public void run() {
		/*
		 * Auffordern des Benutzers zur Eingabe
		 */
		eingabeaufforderung();

		while (!Thread.interrupted()) {
			try {
				/*
				 * thread interrupten wenn betrag bestaetigt, zur neuen eingabe
				 * auffordern.
				 */

				// Versuche NFC Verbindung aufzubauen.
				Card card = cardTerminal.connect("direct");

				// Erzeugen eines Reader/Writer mit ISO7816 Kommunikation (APDU)
				ApduTagReaderWriter readerWriter = new ApduTagReaderWriter(
						new P2PTag(TagType.ISO_DEP, null, card));

				try {
					/*
					 * Erstellt neuen NFC Kommunikator. Da bei P2P Reader und
					 * Writer identisch sind sind beide Parameter readerWriter.
					 */
					Kommunikator kommunikator = new Kommunikator(readerWriter,
							readerWriter);
					kommunikator.verbindeAlsInitiator();
				} catch (Exception exception) {
					/*
					 * Fehlerbehandlung
					 */
					// Bei Exception (zb. Verbindungsabbruch)
					// Karte entfernen/abmelden (logisch, nicht zwingend
					// physisch)
					card.disconnect(true);
					
					/*
					 * Diese Exception ist gewuenscht da sie als Signal zur Kartenabmeldung dient!
					 */

					//exception.printStackTrace();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException exception1) {
						/*
						 * Fehlerbehandlung
						 */
						break;
					}
				}
				/*
				 * Wird aufgerufen wenn Karte entfernt wurde
				 */
				finally {
					/*
					 * Betragseingabe verlangen und Uebergeben des Betrags an
					 * Kasse
					 */

					card.disconnect(true);
					// Warten bis Karte physisch entfernt wird.
					waitForCardAbsent();

					/*
					 * Auffordern des Benutzers zur Eingabe
					 */
					eingabeaufforderung();

				}
			} catch (CardException exception) {
				/*
				 * Fehlerbehandlung
				 */
				exception.printStackTrace();
				break;
			}
		}
	}
}
