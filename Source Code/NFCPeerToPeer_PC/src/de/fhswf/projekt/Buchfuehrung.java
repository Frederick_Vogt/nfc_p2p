/**
 * 
 */
package de.fhswf.projekt;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Klasse die die Buchfuehrung ueber Transaktionen verwaltet.
 */
public class Buchfuehrung {
	
	/**
	 * Methode zum speichern einer Transaktion in der Buchfuehrung(-sdatei).
	 * @param transaktionsdaten Daten der Transaktion in der Form: ID Betrag
	 */
	public static void transaktionSpeichern (String transaktionsdaten)
	{
		try {
			/*
			 * Zweiter parameter von FileWriter (true) haengt Text von println an existierende
			 * Datei an.
			 */
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("transaktionen.txt", true)));
		    out.println(transaktionsdaten + "�");
		    out.close();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}
	
	/**
	 * Methode, die alle gespeicherten Transaktionen formatiert returnt.
	 * 
	 * @return String mit allen gespeicherten Transaktionen.
	 */
	public static String getTransaktionen ()
	{
		String transaktionen = "EMPTY";
		try {
			transaktionen = new String(Files.readAllBytes(Paths.get("transaktionen.txt")));
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		
		return transaktionen;
	}
	
	/**
	 * Methode die alle vorhandenen Transaktionen auf dem Bildschirm ausgibt.
	 */
	public static void printTransaktionen ()
	{
		System.out.print(getTransaktionen().replaceAll(" ", "\t"));
	}

}
