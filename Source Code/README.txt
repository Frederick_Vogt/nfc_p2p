NFCPeerToPeer_PC ist ein Eclipse Java Projekt. Es wird normal in Eclipse importiert. F�r die n�tigen nfctools Bibliotheken, welche nur als Maven Projekte vorliegen, bitte folgende Schritte befolgen:
	1)	Wenn nicht vorhanden das Eclipse Maven Plugin			installieren.
	2)	Die Bibliothek per
			File
			Import
			Projects from Git => Next
			Clone URI => Next
				URI: https://github.com/grundid/nfctools
				=> Next
			=> Next
			=> Next
			Import as general Project => Next
			=> Finish
		in die IDE importieren.
	3)	Rechtsklick auf das nun existierende nfctools 			Projekt
			Import
			Existing Maven Projects => Next
			Haken bei /pom.xml entfernen => Finish

Danach zweite Bibliothek per
			File
			Import
			Projects from Git => Next
			Clone URI => Next
				URI: https://github.com/grundid/nfctools
				=> Next
			=> Next
			=> Next
			Import as general Project => Next
			=> Finish
		impoertieren und rechtsklick auf diese
			Configure
			Convert to Maven Project

Quelle:
	https://groups.google.com/forum/?fromgroups#!topic/nfc-developers/7cTQqolLFu8
	